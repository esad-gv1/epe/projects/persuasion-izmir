# EPE

## Persuasion in the digital era

Content source : *Persuasive Technology: Using Computers to Change What We Think and Do*. (2003), B.J. Fogg, Stanford University, San Francisco, CA. Morgan Kaufmann Publishers.

## Authors

Hugo Lopez - Esad•V, Muzafer Demireva - IUE VCD

# General concept

The layout of this text extensively uses rotations and paragraph special positionning. The challenge of not using a GUI based software for this kind of layout may not be very relevant, but it gives us the proof that it can be done with CSS in web-to-print.
The idea of the text is to emphasis on how digital technologies can change the way see things and presuade us to think in a particular way. This project shows many computer interfaces on the user's scrolling : the more one scroll, the more iconographic objects appears.
By clicking the print button, the printable version is accessed and show new pages at the end : one that show the mouse positions during the website consultation, and an otherone showing the scrolling amount in the shape of a chart.