const mdFilesList = ["md/cover.md", "md/main.md", "md/back.md"];
const mdFilesListAfter = ["md/cover.md", "md/main-print.md", "md/back.md"];
const partsList = ["cover", 'content', "back"];

window.addEventListener('load', () => {
    mdFilesList.forEach((mdFile, index) => {
        const request = new XMLHttpRequest();
        request.open("GET", mdFile);

        request.addEventListener("readystatechange", function (event) {
            if (request.readyState === XMLHttpRequest.DONE && request.status === 200) {
                const response = request.responseText;
                const result = markdownit.render(response);

                document.getElementById(partsList[index]).innerHTML = result;
            }
        });

        request.send();
    });
})

const markdownit = window.markdownit(
    {
        // Options for markdownit
        langPrefix: 'language-fr',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer)
    .use(markdownitSpan)
    .use(markdownItAttrs, {
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });

//CRÉATION DES ÉLÉMENTS POUR LE GRAPHIQUE DE DÉPLACEMENT DE LA SOURIS + SYSTÈME D'ÉCOUTE
var canvasMouse = document.createElement('canvas');
canvasMouse.id = 'graphic-mouse';
canvasMouse.width = window.innerWidth;
canvasMouse.height = window.innerHeight;
canvasMouse.style.position = "fixed";
document.body.appendChild(canvasMouse);

var ctxCanvasMouse = canvasMouse.getContext('2d');
ctxCanvasMouse.lineWidth = 5
var lastX, lastY;

window.addEventListener('mousemove', function (e) {
    var mouseX = e.clientX;
    var mouseY = e.clientY;

    dessinerCroix(canvasMouse, mouseX, mouseY, 15)
});



//CRÉATION DES ÉLÉMENTS POUR LE GRAPHIQUE DU SCROLL + SYSTÈME D'ÉCOUTE
var canvasScroll = document.createElement('canvas');
canvasScroll.id = 'graphic-scroll';
canvasScroll.width = window.innerWidth;
canvasScroll.height = window.innerHeight;
canvasScroll.style.position = "fixed";
document.body.appendChild(canvasScroll); // Ajoute le canvas au corps du document

var ctxCanvasScroll = canvasScroll.getContext('2d');
var diametre = 4
var x = 0
var y = 100

var ratio
var bodyHeight
setTimeout(() => {
    bodyHeight = document.body.clientHeight
    ratio = 35 / bodyHeight
}, 50)


window.addEventListener('scroll', function (e) {
    x += 10
    if (diametre > 3) {
        if (x > window.innerWidth) {
            y += 100
            x = 0
        }
        else if (lastScrollY < window.scrollY) {
            diametre += 1
        } else {
            diametre -= 1
        }

        ctxCanvasScroll.beginPath()
        ctxCanvasScroll.arc(x, y, diametre, 0, 2 * Math.PI)
        ctxCanvasScroll.fill()
        ctxCanvasScroll.closePath()
    }
    if (diametre > 43) {
        diametre = 43
    }
    if (diametre <= 3) {
        diametre = 4
    }


});




//RÉCUPÉRATION DU CANVAS -> JPEG
var globalScore = 0
var imageURLMouse, imageURLScroll, blobMouse, blobScroll, canvasScrollGraphic, canvasMouseGraphic, dataURLMouse, dataURLScroll
document.getElementById('button').addEventListener('mousedown', () => {
    document.getElementById('mosaique').remove()
    // Récupérer le canvas
    canvasMouseGraphic = document.getElementById('graphic-mouse');
    canvasScrollGraphic = document.getElementById('graphic-scroll');

    // Convertir le contenu du canvas en une image JPEG
    dataURLMouse = canvasMouse.toDataURL('image/png', 0.9);
    dataURLScroll = canvasScroll.toDataURL('image/png', 0.9);

    // Créer un objet blob à partir des données URL
    blobMouse = dataURLToBlob(dataURLMouse);
    blobScroll = dataURLToBlob(dataURLScroll);

    // Créer un objet URL à partir du blob
    imageURLMouse = URL.createObjectURL(blobMouse);
    imageURLScroll = URL.createObjectURL(blobScroll);
})

//FONCTION DE PREVIEW DE IMAGES POUR LA PRODUCTION
/*window.addEventListener('click', () => {
    // Récupérer le canvas
    canvasMouseGraphic = document.getElementById('graphic-mouse');
    canvasScrollGraphic = document.getElementById('graphic-scroll');

    // Convertir le contenu du canvas en une image JPEG
    dataURLMouse = canvasMouse.toDataURL('image/png', 0.9);
    dataURLScroll = canvasScroll.toDataURL('image/png', 0.9);

    // Créer un objet blob à partir des données URL
    blobMouse = dataURLToBlob(dataURLMouse);
    blobScroll = dataURLToBlob(dataURLScroll);

    // Créer un objet URL à partir du blob
    imageURLMouse = URL.createObjectURL(blobMouse);
    imageURLScroll = URL.createObjectURL(blobScroll);
    /*
        FONCTION TEST POUR PRÉ-AFFICHAGE IMAGE*/
   /* var mouseTest = document.createElement('img');
    mouseTest.src = imageURLMouse;
    mouseTest.style.width = "200px";
    mouseTest.style.top = "0px"
    mouseTest.style.left = "0px"
    mouseTest.style.position = 'fixed'
    mouseTest.classList.add('test')
    mouseTest.style.border = "2px solid black"
    document.body.appendChild(mouseTest);

    var mouseScroll = document.createElement('img');
    mouseScroll.src = imageURLScroll;
    mouseScroll.style.width = "200px";
    mouseScroll.style.top = "0px"
    mouseScroll.style.left = "225px"
    mouseScroll.style.position = 'fixed'
    mouseScroll.classList.add('test')
    mouseScroll.style.border = "2px solid black"
    document.body.appendChild(mouseScroll);
})*/
function dataURLToBlob(dataURL) {
    var parts = dataURL.split(';base64,');
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;
    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], { type: contentType });
}
//FUNCTION APPARITION DES IMAGES SUR LE FORMAT WEB + CRÉATION DE LA MOSAIQUE
var positionUser
var sourcesImgs = ['teenager', 'camera', 'stereo', 'video-games', 'car', 'card','dvd', 'yamaha', "main", 'runscape', 'plasmla', 'radio-cass', 'man-computer', 'ninja', 'map', 'proto', 'mini', 'atari', 'tv', 'walker', 'smartphone', 'synthe', 'cd-photo', 'game', 'jumelles', 'maestro', 'calculator', 'gopro', 'table', 'recorder', 'ibm', 'flashlight', 'screen', 'alarm', "mac-reaper", "consol", 'micro-pc', 'bandai', 'mac', 'mac-ad', 'colour-tv', 'jaguar', 'child', 'library', 'disc']

for (let index = 0; index <= sourcesImgs.length - 1; index++) {
    mosaique('imgs/' + sourcesImgs[index] + '.jpeg', 'img-' + index, 'mosaique')
}

function mosaique(src, id, destination) {
    var wraperImg = document.createElement('div')
    wraperImg.classList.add('backgroundImg')
    wraperImg.style.width = 'fit-content'
    wraperImg.style.height = 'fit-content'

    var img = document.createElement('img')
    img.classList.add('img-mosaique')
    img.src = src
    img.id = id
    img.dataset.etat = 'hidden'

    document.getElementById(destination).appendChild(wraperImg)
    wraperImg.appendChild(img)
}

/*window.addEventListener('scroll', () => {
    positionUser = window.scrollY
    console.log(positionUser)
    for (let index = 0; index <= sourcesImgs.length; index++) {
        if ((positionUser / 2000) > index) {
            document.getElementById('img-' + index).style.visibility = 'visible'
        }
        if ((positionUser / 2000) < index) {
            document.getElementById('img-' + index).style.visibility = 'hidden'
        }
    }
})*/
var rythmImg
setTimeout(() => {
    var imgPicked;
    rythmImg = document.body.clientHeight / (sourcesImgs.length + 10);
    var lastScrollPositionImg = 300;

    window.addEventListener('scroll', () => {
        var positionUser = window.scrollY;

        if (positionUser > lastScrollPositionImg) {
            lastScrollPositionImg += rythmImg;
            imgPicked = randomIntFromInterval(0, sourcesImgs.length - 1);

            var attempts = 0;
            while (document.getElementById('img-' + imgPicked).dataset.etat === 'visible') {
                imgPicked = randomIntFromInterval(0, sourcesImgs.length - 1);

                // Sortir de la boucle si trop d'essais ont été effectués
                attempts++;
                if (attempts >= sourcesImgs.length) {
                    break;
                }
            }

            if (document.getElementById('img-' + imgPicked).dataset.etat === 'hidden') {
                document.getElementById('img-' + imgPicked).style.visibility = 'visible';
                document.getElementById('img-' + imgPicked).dataset.etat = 'visible';
            }
        }

        if (positionUser < (lastScrollPositionImg - rythmImg)) {
            lastScrollPositionImg -= rythmImg;
            imgPicked = randomIntFromInterval(0, sourcesImgs.length - 1);

            var attempts = 0;
            while (document.getElementById('img-' + imgPicked).dataset.etat === 'hidden') {
                imgPicked = randomIntFromInterval(0, sourcesImgs.length - 1);

                // Sortir de la boucle si trop d'essais ont été effectués
                attempts++;
                if (attempts >= sourcesImgs.length) {
                    break;
                }
            }

            if (document.getElementById('img-' + imgPicked).dataset.etat === 'visible') {
                document.getElementById('img-' + imgPicked).style.visibility = 'hidden';
                document.getElementById('img-' + imgPicked).dataset.etat = 'hidden';
            }
        }
    });

    //PLACEMENT DES PARAGRAPHES
    var paragraphes = document.querySelectorAll('p')
    var h3 = document.querySelectorAll('h3')
    paragraphes.forEach((p) => {
        let placement = tirageAuSort()
        p.style.left = placement + 'vw'
    })

    //ÉCOUTE DES IMAGES
    var bk = document.querySelectorAll('.img-mosaique')
    bk.forEach((img)=>{
        img.addEventListener('mouseover', ()=>{
            if(img.dataset.etat === 'visible'){
                img.parentElement.style.background = '#FF5A13'
            }
        })
        img.addEventListener('mouseout', ()=>{
                img.parentElement.style.background = 'transparent'
        })
    })
}, 500);


//FUNCTION APRÈS AFFICHAGE WEB
function afterPaged() {
    //PLACEMENT DU SCORE DANS LA MARGE MIDDLE RIGHT
    /*var selectedMargins = document.querySelectorAll('.pagedjs_margin-right-middle')
    for (let i = 0; i < selectedMargins.length; i++) {
        selectedMargins[i].children[0].innerHTML = globalScore
    }
    //var root = document.querySelector(':root')
    //root.style.setProperty('--pagedjs-bleed-right-right', '12mm')

    //for the TRANCHE -----------------------------------
    /*var nbPages = document.querySelectorAll('.pagedjs_page').length

    var gap = 3.1
    for (let i = 1; i <= nbPages; i++) {
        var target = document.getElementById('page-' + i)
        var childTarget = target.children[0].children[4].children[3].children[1].children[0]
        console.log(childTarget)
        childTarget.style.transform = 'translateY(70mm) translateX(' + (gap * i) + '%) rotate(-90deg)'
        console.log('oui')
    }*/

    for (let index = 0; index <= sourcesImgs.length - 8; index++) {
        if(index < (sourcesImgs.length/2)-8){
            mosaique('imgs/' + sourcesImgs[index] + '.jpeg', 'img-' + index, 'reception-imgs')
        } else {
            mosaique('imgs/' + sourcesImgs[index] + '.jpeg', 'img-' + index, 'reception-imgs-2')
        }
    }

    document.getElementById('img-20').parentElement.remove()
    document.getElementById('img-21').parentElement.remove()
    document.getElementById('img-22').parentElement.remove()
}

//AFFICHAGE TO PRINT
document.getElementById('button').addEventListener('mouseup', init)
function init() {
    rythmImg = 0
    changeLinkSource("second-style-sheet", "css/cover-muza.css");
    changeLinkSource("main-style-sheet", "css/style-muza.css");
    canvasMouseGraphic.remove();
    canvasScrollGraphic.remove();
    document.getElementById('button').style.display = 'none';

    // Création d'une seule requête pour tous les fichiers Markdown
    const requests = mdFilesListAfter.map(mdFileAfter => {
        return new Promise((resolve, reject) => {
            const requestAfter = new XMLHttpRequest();
            requestAfter.open("GET", mdFileAfter);

            requestAfter.addEventListener("readystatechange", function (event) {
                if (requestAfter.readyState === XMLHttpRequest.DONE && requestAfter.status === 200) {
                    const responseAfter = requestAfter.responseText;
                    const resultAfter = markdownit.render(responseAfter);
                    resolve(resultAfter); // Résoudre la promesse avec le résultat Markdown traité
                }
            });
            requestAfter.send();
        });
    });

    // Attendre que toutes les requêtes soient terminées
    Promise.all(requests).then(results => {
        // Supposons que partsList contient les ID des éléments où vous souhaitez ajouter le contenu Markdown
        
        document.getElementById('content').innerHTML = results[1]; // Ajouter le résultat Markdown au bon endroit dans le HTML
            

        // Une fois que les éléments Markdown sont placés dans le HTML, vous pouvez faire d'autres actions
        loadScript("vendors/js/paged.polyfill.js");

        const pagedCss = document.createElement("link");
        pagedCss.href = "vendors/css/paged-preview.css";
        pagedCss.type = "text/css";
        pagedCss.rel = "stylesheet";
        document.head.appendChild(pagedCss);

        var wraperMouse = document.createElement('div')
        wraperMouse.classList.add('wraper-graphique')
        document.getElementById('content').appendChild(wraperMouse);

        var imgMouse = document.createElement('img');
        imgMouse.src = imageURLMouse;
        imgMouse.classList.add('graphique', 'rotate-image')
        imgMouse.style.width = "200mm";
        wraperMouse.appendChild(imgMouse);

        var pMouse = document.createElement('p')
        pMouse.innerHTML = 'This graphic has been made by your mouse positions'
        pMouse.id = 'legend-graphique-souris'
        wraperMouse.appendChild(pMouse)


        var wraperScroll = document.createElement('div')
        wraperScroll.classList.add('wraper-graphique')
        document.getElementById('content').appendChild(wraperScroll);

        var imgScroll = document.createElement('img');
        imgScroll.src = imageURLScroll;
        imgScroll.classList.add('graphique', 'rotate-image')
        imgScroll.style.width = "200mm";
        document.getElementById('content').appendChild(imgScroll);

        var pScroll = document.createElement('p')
        pScroll.innerHTML = 'This graphic has been made by your scroll behavior.'
        pScroll.id = 'legend-graphique-souris'
        wraperScroll.appendChild(pScroll)

        setTimeout(afterPaged(), 300)
        console.log('Markdown traité et placé dans le HTML');
    }).catch(error => {
        // Gérer les erreurs éventuelles
        console.error('Une erreur est survenue lors du chargement des fichiers Markdown:', error);
    });

}





//init();

//utils functions

// Load a script with a promise
function loadScript(src) {
    return new Promise(function (resolve, reject) {
        const script = document.createElement('script');
        script.src = src;

        script.onload = () => resolve(script);
        script.onerror = () => reject(new Error(`Script load error for ${src}`));

        document.body.appendChild(script);
    });
}



// setTimeout(() => {
//     // rotateDivs();
// }, 1200)
// function rotateDivs(){
//     let Divs = document.querySelectorAll('div')
//     Divs.forEach(box => {
//       box.addEventListener('click', () => {

//       })  
//     })
// }

var lastScroll = sessionStorage.getItem('scrollPosition')
window.addEventListener('mousedown', () => {
    scrollPosition = window.scrollY
    sessionStorage.setItem('scrollPosition', scrollPosition);
    lastScroll = sessionStorage.getItem('scrollPosition')
})

window.addEventListener('keydown', () => {
    scrollPosition = window.scrollY
    sessionStorage.setItem('scrollPosition', scrollPosition);
    lastScroll = sessionStorage.getItem('scrollPosition')
})



function tirage() {
    let orientation
    switch (randomIntFromInterval(1, 4)) {
        case 1: orientation = '90deg'
            break;
        case 2: orientation = '-90deg'
            break;
        case 3: orientation = '-180deg'
            break;
        case 4: orientation = '0deg'
            break;
    }
    return orientation
}
function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

function changeLinkSource(linkId, newUrl) {
    var linkElement = document.getElementById(linkId);
    if (linkElement) {
        linkElement.href = newUrl;
    } else {
        console.error("La balise link avec l'ID " + linkId + " n'existe pas.");
    }
}

function tirageAuSort() {
    var numeroCas = Math.floor(Math.random() * 3);

    switch (numeroCas) {
        case 0:
            return 10;
        case 1:
            return 35;
        case 2:
            return 55;
    }
}

function tirageAuSortH() {
    var numeroCas = Math.floor(Math.random() * 2);

    switch (numeroCas) {
        case 0:
            return 10;
        case 1:
            return 90;
    }
}

// Définir la valeur initiale et maximale de l'axe de la variable font
const initialValue = 10;
const maxValue = 300;
let lastScrollY = 0; // Stocker la position de défilement précédente

// Fonction pour ajuster l'axe de la variable font
function adjustFontAxis() {
    // Récupérer la position de défilement verticale
    let scrollY = window.scrollY;

    // Calculer le déplacement vertical depuis le dernier événement de défilement
    let deltaY = scrollY - lastScrollY;

    // Calculer la nouvelle valeur de l'axe en ajoutant le déplacement vertical
    let newValue = Math.min(maxValue, Math.max(initialValue, parseFloat(getComputedStyle(document.body).getPropertyValue('--font-axis')) + deltaY / 5));

    // Appliquer la nouvelle valeur de l'axe à la variable font
    document.body.style.setProperty('--font-axis', newValue);

    // Mettre à jour la position de défilement précédente pour le prochain événement de défilement
    lastScrollY = scrollY;
}


// Ajouter un écouteur d'événement pour le défilement de la fenêtre
window.addEventListener('scroll', () => {
    // Appeler la fonction pour ajuster l'axe de la variable font
    adjustFontAxis();
});

function dessinerCroix(canvas, x, y, taille) {
    // Récupérer le contexte 2D du canvas
    var ctx = canvas.getContext("2d");

    // Dessiner la ligne horizontale de la croix
    ctx.beginPath();
    ctx.moveTo(x - taille, y);
    ctx.lineTo(x + taille, y);
    ctx.stroke();

    // Dessiner la ligne verticale de la croix
    ctx.beginPath();
    ctx.moveTo(x, y - taille);
    ctx.lineTo(x, y + taille);
    ctx.stroke();
}


onbeforeprint = (event) => {
    //console.log(event.timeStamp)
    setTimeStampNow();
    }
function setTimeStampNow(){
    const currentDate=new Date()
    const milliseconds = currentDate.getMilliseconds();
    //console.log(milliseconds);
    const rightnow=currentDate.toLocaleString();
    document.getElementById("timestamp").innerHTML=rightnow+" — "+milliseconds+"ms Izmir";
}
document.getElementById('button').addEventListener('click',()=>{
    setTimeout(()=>{
        setTimeStampNow();
        console.log('stamp')
    }, 1500)
})