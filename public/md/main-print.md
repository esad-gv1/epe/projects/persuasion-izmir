## INTRODUCTION <span class="h2b">               Въведение</span>

::: dPG
### PERSUASION IN THE DIGITAL ERA <span class="h3b">УБЕЖДАВАНЕ В ДИГИТАЛНАТА ЕРА</span>{.rotated}
:::

Computers weren't initially created to persuade; they were built for handling data — calculating, storing, and retrieving. But as computers have migrated from research labs onto desktops and into everyday life, they have become more persuasive by design. Today computers are taking on a variety of roles as persuaders, including roles of influence that traditionally were filled by teach­ers, coaches, clergy, therapists, doctors, and salespeople, among others. We have entered an era of persuasive technology, of interactive computing systems designed to change people's attitudes and behaviors.{.middleClass}

::: secondParagraph
The earliest signs of persuasive technology appeared in the 1970s and 1980s, but one of the earliest examples is a computer system named Body Awareness Resource Network (BARN), developed in the late 1970s. But it wasn't until the late 1990s — specifically, the emergence of the Internet — that more than a handful of people began creating persuasive technology.{.leftClass}
:::


::: secondH3
### PERSUASION ON <br>THE WEB<br> <span class="h3b">УБЕЖДАВАНЕ В<br>мрежата</span>
:::

::: thirdParagraph
The emergence of the Internet has led to a proliferation of Web sites designed to persuade or motivate people to change their attitudes and behavior. Web sites are the most common form of persuasive technology today. Consider a few examples\:
:::

::: aParagraph
   Amazon.com doesn't just process orders; it attempts to persuade people to purchase more products. It does so by offering suggestions based on user preferences gathered during previous visits and feedback from others who ordered the product, and by presenting compelling promotions, such as the Gold Box offers and the "Share the Love" program.

   Iwon.com wants visitors to make it their default search engine and awards prizes to persuade them to do so.
:::

::: secondH3
### <br><br><br><br><br><br><br>BEYOND THE WEB <span class="h3b">             извън мрежата</span>
:::

:::{.bParagraph}
Beyond the Web, persuasive technology can take on many forms, from mobile phones to "smart" toothbrushes to the computerized trailers that sit by the roadside and post the speed of passing cars in an attempt to persuade drivers to abide by the speed limit. In some cases, the technology may not even be visible to the user.
:::

::: cParagraph
The uses for persuasive technology also will expand in the coming decade, extending far beyond the primary applications we see today, such as advertis­ing, marketing, and sales. At work, persuasive technology might be used to motivate teams to set goals and meet deadlines. At home, it could encourage kids to develop better study habits. In civic life, it could persuade people to vote on election day. Wherever the need for persuasion exists, I believe that interac­tive technology can play a role.
:::

::: secondH3
## OVERVIEW OF CAPTOLOGY <span class="h2b"><hr style="height:155px; visibility:hidden;"/>Преглед на <br>каптология</span>
:::

::: ePG
### DEFINING PERSUASION <span class="h3b">                        дефиниция на убеждаване</span>
:::

::: fPG
Although philosophers and scholars have been examining persuasion for at least 2,000 years, not everyone agrees on what the term really means.' For pur­poses of captology, I define persuasion as an attempt to change attitudes or behaviors or both (without using coercion or deception). 
:::

::: gPG
It's important to note the difference between persuasion and coercion, terms that are sometimes confused. Coercion implies force; while it may change be­haviors, it is not the same as persuasion, which implies voluntary change — in behavior, attitude, or both.
:::

::: secondH3 
::: hPG
### LEVELS OF PERSUASION\: <br>MACRO AND MICRO <br><span class="h3b">нива на убеждаване\:</span><br><br><br><br><br><br> <span class="h3b">макро и микро</span></span>
:::
:::

::: iPG
Attitude and behavior changes that result from successful persuasion can take place on two levels\: macro and micro. Understanding these two levels of per­suasion will make it easier to identify, design, or analyze persuasion opportuni­ties in most computing products.
:::

::: kPG
A game called HIV Roulette is designed to persuade users to avoid risky sexual behavior. Baby Think It Over, is designed to persuade teenage girls to avoid be­coming pregnant. Persuasion and motivation are the sole reasons such prod­ucts exist. I use the term macrosuasion to describe this overall persuasive intent of a product.
:::

::: jPG
Microsuasion elements can be designed into dialogue boxes, icons, or inter­action patterns between the computer and the user.
:::

::: secondH3
### MICROSUASION <br>ON THE WEB <span class="h3b">микросуазия в мрежата</span>
:::

::: lPG
Examples of Web sites that use microsuasion are plentiful and sometimes sub­tle. For example, eBay has created a rating system — what it calls "feedback" — whereby buyers and sellers evaluate each other after a transaction is com­pleted. This system motivates people to be honest, responsive, and courteous in their interactions. 
:::

::: mPG
### MICROSUASION <br>IN VIDEO GAMES <span class="h3b">микросуазия във видео игри</span>

::: nPG
Video games are exceptionally rich in microsuasion elements. The overall goal of most games is to provide entertainment, not to persuade. But during the en­tertainment experience, players are bombarded with microsuasion elements, sometimes continuously, designed to persuade them to keep playing.
:::

::: secondH3
## THE FUNCTIONAL TRIAD <span class="h2b">функционалната<br>триада</span>
:::

### <hr style="height:55px; visibility:hidden;" />COMPUTERS AS TOOLS <span class="h3b">компютрите като инструменти</span>

::: oPG
One basic function of computers is to serve as tools. This is the first corner of the functional triad. In their role as tools, the goal of computing products is to make activities easier or more efficient to do or to do things that would be virtually impossible without technology.
:::


::: secondH3
::: pPG
### COMPUTERS AS MEDIA <span class="h3b">компютрите като медии</span>
:::
:::

::: qPG
Computers also function as media — a role that has grown in recent years as processing power has increased and networking has become common. There are two categories of computers as media\: symbolic and sensory. Computers function as symbolic media when they use symbols to convey information (for example, text, graphics, charts, and icons).
:::

::: rPG
### COMPUTERS AS SOCIAL ACTORS <span class="h3b">компютрите като<BR>социални актьори</span>
:::

::: sPG
The third corner of the functional triad depicts the role that computers play as social actors or living entities. When people use an interactive technology, they often respond as though it were a living being.
:::

::: secondH3
### RESEARCH AND DESIGN APPLICATIONS <span class="h3b">изследователски и <br>дизайнерски приложения</span>
:::

::: tPG
For those with an interest in researching or designing persuasive technologies, the functional triad provides a framework for sorting out the elements in the overall user experience of a product. For researchers, identifying which elements of the product are acting as a tool, medium, social actor, or some combination of the three roles, makes it easier to understand the nature of the product's persuasive power. In my experience, this simple step often brings clarity to a research or analysis project and provides a basis for further exploration. 
:::

::: uPG
When exploring ideas for a new product, designers can ask themselves how the product might persuade as a tool, medium, social actor, or through a combination of roles.
:::

::: secondH3
::: vPG
### COMPUTERS AS PERSUASIVE MEDIA <span class="h3b">компютрите като убеждаващи медии</span>
:::
:::

::: wPG
When it comes to shaping attitudes and behavior, experience makes a difference. Those in the business of persuading understand and apply this principle. AOL gives out trial memberships on CD. Auto dealers encourage customers to take a test drive. 

Government programs send at-risk kids to visit correctional facilities to get a glimpse of prison life. The goal in these and other scenarios is to provide a compelling experience that will persuade people to change their attitudes or behaviors.
:::

::: xPG
The experience principle can be applied to persuasive technologies as well. When computers are used as persuasive media — particularly when they are used to create simulations — they can have a powerful impact on shaping atti­tudes and behaviors in the real world. This chapter will focus on computers as persuasive media—the second corner of the functional triad.
:::


::: secondH3
### PERSUADING THROUGH COMPUTER <br><br><br>SIMULATION <span class="h3b">убеждаване чрез компютърна <br><br><br>симулация</span>
:::

::: yPG
From the standpoint of persuasion, the technological elements of a simulation are less important than what the user actually experiences. Drawing on how people experience computer simulations, I propose three categories of simula­tion that are relevant to persuasive technologies\: Simulated cause-and-effect scenarios; Simulated environments; Simulated objects; Within each of these categories, theories from social science — especially psychology — offer insight into computers as persuasive sensory media. The pages that follow discuss each type of simulation in turn.
:::


::: secondH3
### <hr style="height:300px; visibility:hidden;" />CREDIBILITY AND THE WORLD WIDE WEB <span class="h3b"><hr style="height:100px; visibility:hidden;" />правдоподобност и <BR>световната мрежа</span>
:::

::: zPG
If you look at the most frequently visited Web sites, you'll see that many sites seek to persuade users in some way. MSN and other leading portal sites such as AOL and Yahoo try to convince users to do their Web searching, shopping, and chatting with friends on their sites or those of their affiliates. They also hope that users will register with them — create a personalized homepage, such as My MSN, My AOL, or My Yahoo — giving the site operators information about users and providing a way to contact them directly in the future.
:::

::: secondH3
### VARIABILITY OF WEB CREDIBILITY <hr style="height:225px; visibility:hidden;" /><span class="h3b">променливост на уеб<BR>правдоподобност</span>
:::

::: aaPG
The Web can be a highly credible source of information, depending on the per­son or organization behind a given Web site (the site "operator"). The Web also can be one of the least credible information sources. Many pages on the Web reflect incompetence, and some are pure hoaxes. You've probably been to Web sites that not only lacked expertise, but seem designed to deceive.
:::

::: bbPG
Because fewbarriers prevent people from publishing on the Web, you’ll find deceptive coverage of current events,health information that is factually incorrect, and ads that promise the impossible. One notable example of bogus information online was designed to teach investors about online fraud and persuade people to be more skeptical <br>about what they find online. 
:::

::: secondH3
### TWO SIDES OF WEB CREDIBILITY  <hr style="height:225px; visibility:hidden;" /><span class="h3b">
:::

::: ddPG
### <span class="h3b">две страни на уеб
:::


::: ccPG
### <span class="h3b">правдоподобността</span>
:::

::: eePG
Web credibility has two sides, one that relates mostly to Web surfers and one that relates to Web designers. On the one side, credibility is impor­tant to those who use the Web. People need to evaluate if Web sources are credi­ble or not. This issue of "information quality" has been embraced by librarians and teachers. 
:::

::: ffPG
Fortunately, many good guidelines already exist to help students and researchers evaluate the information they find online. The other aspect of Web credibility relates primarily to Web site designers. The main issue for designers is how to create Web sites that convey appro­priate levels of credibility.
:::


::: secondH3
### <hr style="height:60px; visibility:hidden;" />THE STANFORD WEB CREDIBILITY STUDIES <hr style="height:150px; visibility:hidden;" /><span class="h3b">проучванията на станфорд <BR>за уеб правдоподобност</span>
:::

::: ggPG
Over the past four years, my Stanford Persuasive Technology Lab has con­ducted a number of research studies related to Web credibility, involving over 6,000 participants.
:::

::: hhPG
After conducting a few smaller experiments and surveys, in the fall of 1999, 10 graduate students and I launched a large-scale research project to investigate Web credibility.
In 1999, after the graduate students and I reviewed the few existing pieces of literature on Web credibility and surfed many Web sites, taking notes, our team identified more than 300 elements that could affect the way users per­ceived the credibility of a given Web site.
:::


::: secondH3
::: iiPG
### PERSUASION THROUGH <BR>MOBILE TECHNOLOGY <br><span class="h3b">убеждаване чрез</span> <BR><span class="h3b">мобилна технология</span>
:::
:::

::: jjPG
In the last few years, the idea of mobile commerce has been a darling of tech­nology companies, marketing agencies, and the news media. Some forms of mobile commerce are taking shape in Asia and Europe while the U.S. lags behind. One vision for mobile commerce is to provide people with opportuni­ties to buy things conveniently and when the need arises.
:::

::: kkPG
My vision for mobile persuasion goes beyond using mobile devices to promote products and services. I believe mobile systems can and will be used to promote health, safety, community involvement, personal improve­ment, and more. When you pack a mobile persuasive technology with you, you pack a source of influence. At any time (ideally, at the appropriate time), the device can suggest, encourage, and reward; it can track your performance or lead you through a process; or it can provide compelling factual evidence or insightful simulations.
:::

::: secondH3
         

### B.J. Fogg, Ph. D., Stanford University. Persuasive Technology\: Using Computers to Change What We Think and Do. (2003). San Francisco, CA. Morgan Kaufmann Publishers.<BR><BR>Hugo Lopez<BR>Muzafer Demireva<BR><BR><span id='timestamp'></span><BR><BR>EPE Workshop Propagation 3<BR><BR><br>
:::

<div id='reception-imgs'></div>
<div id='reception-imgs-2'></div>



